//
//  ViewController.swift
//  VCApp
//
//  Created by Alexander Kononok on 9/4/21.
//

import UIKit

class ViewController: UIViewController {

    private let mainView = UIView()
    private let textLabel = UILabel()
    private let languagesPicker = UIPickerView()
    private let buttonsView = UIView()
    private let lighThemeButton = UIButton()
    private let darkThemeButton = UIButton()
    private let autoThemeButton = UIButton()
    
    private var appleImageView = UIImageView()
    private var languagesArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        view.addSubview(mainView)
        
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.topAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mainView.leftAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        mainView.rightAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        mainView.bottomAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        mainView.backgroundColor = .systemBackground
        
        // MARK: - Apple icon
        let appleImage = UIImage(named: "ic_apple")
        appleImageView = UIImageView(image: appleImage)
        appleImageView.contentMode = .scaleAspectFit
        mainView.addSubview(appleImageView)
        
        appleImageView.translatesAutoresizingMaskIntoConstraints = false
        appleImageView.topAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        appleImageView.leftAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.leftAnchor).isActive = true
        appleImageView.widthAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.widthAnchor).isActive = true
        appleImageView.heightAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.heightAnchor, multiplier: 0.10).isActive = true
        
        // MARK: - Text label
        textLabel.text = NSLocalizedString("ViewController.textLabel",
                                           comment: "Text for label.")
        textLabel.textAlignment = .center
        textLabel.numberOfLines = 0
        textLabel.lineBreakMode = .byTruncatingTail
        textLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        mainView.addSubview(textLabel)
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.topAnchor.constraint(equalTo: appleImageView.bottomAnchor).isActive = true
        textLabel.leftAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.leftAnchor).isActive = true
        textLabel.widthAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.widthAnchor).isActive = true
        textLabel.heightAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.heightAnchor, multiplier: 0.30).isActive = true
        
        // MARK: - Languages picker
        languagesPicker.dataSource = self
        languagesPicker.delegate = self
        mainView.addSubview(languagesPicker)
        languagesArray = [
            NSLocalizedString("ViewController.picker.item.english", comment: "English language"),
            NSLocalizedString("ViewController.picker.item.russian", comment: "Russian language"),
            NSLocalizedString("ViewController.picker.item.belarusian", comment: "Belarusian language"),
        ]
        
        languagesPicker.translatesAutoresizingMaskIntoConstraints = false
        languagesPicker.topAnchor.constraint(equalTo: textLabel.bottomAnchor).isActive = true
        languagesPicker.leftAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.leftAnchor).isActive = true
        languagesPicker.widthAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.widthAnchor).isActive = true
        
        // MARK: - View for buttons
        mainView.addSubview(buttonsView)
        
        buttonsView.translatesAutoresizingMaskIntoConstraints = false
        buttonsView.topAnchor.constraint(equalTo: languagesPicker.bottomAnchor).isActive = true
        buttonsView.leftAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.leftAnchor).isActive = true
        buttonsView.widthAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.widthAnchor).isActive = true
        buttonsView.bottomAnchor.constraint(
            equalTo: mainView.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        // MARK: - Light theme button
        let cornerRadiusButton: CGFloat = 10
        lighThemeButton.setTitle(NSLocalizedString("ViewController.button.title.light",
                                                   comment: "Title for button"), for: .normal)
        lighThemeButton.addTarget(
            self, action: #selector(self.lightThemeButtonPressed), for: .touchUpInside)
        lighThemeButton.layer.cornerRadius = cornerRadiusButton
        lighThemeButton.backgroundColor = .systemPurple
        lighThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        buttonsView.addSubview(lighThemeButton)
        
        lighThemeButton.translatesAutoresizingMaskIntoConstraints = false
        lighThemeButton.centerYAnchor.constraint(
            equalTo: buttonsView.centerYAnchor).isActive = true
        lighThemeButton.leftAnchor.constraint(
            equalTo: buttonsView.leftAnchor,constant: 10).isActive = true
        
        // MARK: - Dark theme button
        darkThemeButton.setTitle(
            NSLocalizedString("ViewController.button.title.dark",
                              comment: "Title for button"), for: .normal)
        darkThemeButton.addTarget(self, action: #selector(self.darkThemeButtonPressed),
                                  for: .touchUpInside)
        darkThemeButton.layer.cornerRadius = cornerRadiusButton
        darkThemeButton.backgroundColor = .systemPurple
        darkThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        buttonsView.addSubview(darkThemeButton)
        
        darkThemeButton.translatesAutoresizingMaskIntoConstraints = false
        darkThemeButton.leftAnchor.constraint(
            equalTo: lighThemeButton.rightAnchor, constant: 10).isActive = true
        darkThemeButton.centerYAnchor.constraint(
            equalTo: buttonsView.centerYAnchor).isActive = true
        darkThemeButton.widthAnchor.constraint(
            equalTo: lighThemeButton.widthAnchor, multiplier: 1).isActive = true
        
        // MARK: - System theme button
        autoThemeButton.setTitle(NSLocalizedString("ViewController.button.title.auto",
                                                   comment: "Title for button"), for: .normal)
        autoThemeButton.addTarget(
            self, action: #selector(self.autoThemeButtonPressed), for: .touchUpInside)
        autoThemeButton.layer.cornerRadius = cornerRadiusButton
        autoThemeButton.backgroundColor = .systemPurple
        autoThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        buttonsView.addSubview(autoThemeButton)
        
        autoThemeButton.translatesAutoresizingMaskIntoConstraints = false
        autoThemeButton.leftAnchor.constraint(
            equalTo: darkThemeButton.rightAnchor, constant: 10).isActive = true
        autoThemeButton.rightAnchor.constraint(
            equalTo: buttonsView.rightAnchor, constant: -10).isActive = true
        autoThemeButton.centerYAnchor.constraint(
            equalTo: buttonsView.centerYAnchor).isActive = true
        autoThemeButton.widthAnchor.constraint(
            equalTo: darkThemeButton.widthAnchor, multiplier: 1).isActive = true
        
    }
    
    @objc func lightThemeButtonPressed() {
        mainView.backgroundColor = .white
        appleImageView.image = UIImage(named: "ic_apple_black")
        textLabel.textColor = .black
        languagesPicker.setValue(UIColor.black, forKey: "textColor")
        darkThemeButton.backgroundColor = .systemPurple
        autoThemeButton.backgroundColor = .systemPurple
        lighThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 26)
        darkThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        autoThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            self.lighThemeButton.backgroundColor = .systemOrange
        }
        
    }
    
    @objc func darkThemeButtonPressed() {
        mainView.backgroundColor = .black
        appleImageView.image = UIImage(named: "ic_apple_white")
        textLabel.textColor = .white
        languagesPicker.setValue(UIColor.white, forKey: "textColor")
        lighThemeButton.backgroundColor = .systemPurple
        autoThemeButton.backgroundColor = .systemPurple
        lighThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        darkThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 26)
        autoThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            self.darkThemeButton.backgroundColor = .systemOrange
        }
    }
    
    @objc func autoThemeButtonPressed() {
        mainView.backgroundColor = .systemBackground
        appleImageView.image = UIImage(named: "ic_apple")
        textLabel.textColor = UIColor(named: "basicTextColor")
        lighThemeButton.backgroundColor = .systemPurple
        darkThemeButton.backgroundColor = .systemPurple
        lighThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        darkThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        autoThemeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 26)
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            self.autoThemeButton.backgroundColor = .systemOrange
        }
        
        if self.traitCollection.userInterfaceStyle == .light {
            languagesPicker.setValue(UIColor(named: "basicTextColor"), forKey: "textColor")
        } else {
            languagesPicker.setValue(UIColor(named: "basicTextColor"), forKey: "textColor")
        }
    }
    

}

extension ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languagesArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(languagesArray[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) { }
}

